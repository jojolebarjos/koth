
# King of the Hill #

Cubes are nasty little dudes. They want to fight. And they need you. Welcome, commander.

### Rules

In this turn-based strategy game, a player (called a team) has a fixed number of action points per turn.
Each cube (called a pawn) can move to adjacent tiles and change stance.
They starts with some health points and disappears if their health reaches zero or fall out of the grid.
Three stance are available: rock, paper and scissors.
When a pawn tries to move on another cube (ally or foe), a fight occurs.

* If they have the same stance, the attacker pushes the defender.
* If the attacker is stronger than the defender (for instance, rock against scissors), the defender loses one health point.
* On the other hand, if the defender is stronger than the attacker, the latter loses one health point. But, to balance this sacrifice, the defender is pushed twice.

### What to do

The idea behind this project is to implement artificial intelligences and see which one wins.
To do that, you only have to implement the Java interface `AI`.
You have to create your own package inside `koth.user`.

```
#!java
package koth.user.bar;

import koth.game.*;

public class Foo implements AI {
	
	private int team;
	
	// This is called at startup. You get the initial game, your team ID and the rules.
	@Override
	public void initialize(Game game, int team, Rules rules) {
		// You will usually want to remember at least your team ID.
		this.team = team;
	}
	
	// When it is your turn, as long as you have action points left, this will be called to ask for a move.
	@Override
	public Action play(Game game, int actions) {
		// You have to create a clever action.
		Pawn pawn = game.getPawnByIndex(team, 0);
		if (game.isFree(pawn.getLocation().add(Move.North)))
			return new Action(pawn, Stance.Rock, Move.North);
		// If you return null or throw an exception, you skip your turn.
		return null;
	}
	
}
```

You can see what other people have already done. Be imaginative!
